import React, { useState } from "react";
import MobileMenuNav from "./MobileMenuNav";
import MobileSearch from "./MobileSearch";
function Header() {
  /**
   * variables of mobile menu ans search here
   */
  const [menuOpen, setMenuOpen] = useState(false);
  const [searchOpen, setSearchOpen] = useState(false);

  /**
   * Menu toggle open and close
   */
  const toggleMenu = () => {
    setMenuOpen(!menuOpen);
  };

  /**
   * Search toggle open and close
   */
  const toggleSearch = () => {
    setSearchOpen(!searchOpen);
  };

  return (
    <header className="bg-white fixed w-full top-0 ">
      <div className="shadow items-center antialiased p-2 flex justify-between">
        <div className="flex" onClick={toggleMenu}>
          <button className="hover:cursor-pointer focus:outline-none focus:text">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              viewBox="0 0 24 24"
              className=" w-6 h-6"
              stroke="currentColor"
              strokeWidth="2"
              strokeLinecap="round"
              strokeLinejoin="round"
            >
              <line x1="3" y1="12" x2="21" y2="12"></line>
              <line x1="3" y1="6" x2="21" y2="6"></line>
              <line x1="3" y1="18" x2="21" y2="18"></line>
            </svg>
          </button>
        </div>
        <div>
          <img className=" h-10 w-auto" src="/assets/logo/logo.png" />
        </div>
        <button className="hover:cursor-pointer" onClick={toggleSearch}>
          <svg
            width="32"
            height="30"
            viewBox="0 0 32 30"
            className="fill-current h-6 w-6"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path d="M20.6401 21.4792C18.0684 23.3521 14.832 24.2356 11.5899 23.9499C8.34775 23.6642 5.34365 22.2308 3.18927 19.9415C1.03488 17.6522 -0.107786 14.6792 -0.00607837 11.6278C0.0956297 8.57638 1.43407 5.67603 3.73671 3.51731C6.03935 1.35858 9.13305 0.103793 12.3879 0.00844202C15.6427 -0.0869093 18.814 0.984344 21.2559 3.00408C23.6978 5.02381 25.2268 7.84016 25.5315 10.8797C25.8362 13.9192 24.8938 16.9533 22.8961 19.3642L31.4561 27.3592L29.1841 29.4892L20.6561 21.4792H20.6401ZM12.8001 20.9992C14.0608 20.9992 15.3091 20.7664 16.4738 20.3141C17.6386 19.8618 18.6969 19.1989 19.5883 18.3632C20.4797 17.5274 21.1869 16.5353 21.6693 15.4434C22.1518 14.3514 22.4001 13.1811 22.4001 11.9992C22.4001 10.8173 22.1518 9.64699 21.6693 8.55506C21.1869 7.46313 20.4797 6.47098 19.5883 5.63525C18.6969 4.79953 17.6386 4.13659 16.4738 3.6843C15.3091 3.23201 14.0608 2.99921 12.8001 2.99921C10.254 2.99921 7.8122 3.94743 6.01185 5.63525C4.2115 7.32308 3.20008 9.61227 3.20008 11.9992C3.20008 14.3862 4.2115 16.6753 6.01185 18.3632C7.8122 20.051 10.254 20.9992 12.8001 20.9992V20.9992Z" />
          </svg>
        </button>
      </div>
      {menuOpen && <MobileMenuNav toggleMenu={toggleMenu} />}
      {searchOpen && <MobileSearch toggleSearch={toggleSearch} />}
    </header>
  );
}

export default Header;
