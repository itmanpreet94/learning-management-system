import React from "react";

function Logo() {
  return (
    <div className="relative flex justify-center w-full py-2 border border-b border-gray-200">
      <img className=" h-10 w-auto" src="/assets/logo/logo.png" />
    </div>
  );
}

export default Logo;
