import React from "react";
import Link from "next/link";
import Logo from "./Logo";

function MobileMenuNav(props) {
  return (
    <div>
      <nav className="w-full absolute top-0 bg-white flex flex-col flex-1 h-screen overflow-y-hidden">
        <div className="relative flex items-center">
          <div className="py-3 px-2 items-center absolute z-40">
            <div className="">
              <button
                className="hover:cursor-pointer flex items-center "
                onClick={props.toggleMenu}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="feather feather-x"
                >
                  <line x1="18" y1="6" x2="6" y2="18"></line>
                  <line x1="6" y1="6" x2="18" y2="18"></line>
                </svg>
              </button>
            </div>
          </div>

          <Logo />
        </div>
        <div className="text-sm border-b border-gray-400 text-gray-700">
          <Link href="#">
            <a className="px-2 py-1 block font-semibold ">Home</a>
          </Link>
          <Link href="#">
            <a className="px-2 py-1 block font-semibold">About Us</a>
          </Link>
          <Link href="#">
            <a className="px-2  py-1 block font-semibold">Contact Us</a>
          </Link>
        </div>

        <div>
          <div className="flex items-center py-2 px-2">
            <img
              className="h-12 w-12 rounded-full object-cover border-2 border-gray-500"
              src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=100&q=80"
              alt="profile img"
            />
            <span className="ml-2 font-semibold text-gray-700">
              Isha Ambani
            </span>
          </div>
          <div className="text-sm text-gray-700">
            <Link href="#">
              <a className="px-2 py-1 block font-semibold flex justify-between">
                Account Setting
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="feather feather-chevron-right"
                >
                  <polyline points="9 18 15 12 9 6"></polyline>
                </svg>
              </a>
            </Link>

            <Link href="#">
              <a className="px-2 py-1 block font-semibold ">Support</a>
            </Link>
            <Link href="#">
              <a className="px-2 py-1 block font-semibold ">Sign-Out</a>
            </Link>
          </div>
        </div>
      </nav>
    </div>
  );
}

export default MobileMenuNav;
