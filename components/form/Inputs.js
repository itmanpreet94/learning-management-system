import React, { useState } from "react";
import { OpenEyeSvg, CloseEyeSvg } from "../svg";
export function Textinputs(props) {
  /**
   * input type variable
   *
   */
  const [type, setType] = useState(props.type);

  /**
   * svg css classes
   *
   */
  const [svgClassRight, setSvgClassRight] = useState(
    "absolute inset-y-0 right-0 flex items-center"
  );

  const [svgClassLeft, setSvgClassLeft] = useState(
    "absolute inset-y-0 left-0 flex items-center"
  );

  /**
   * toggle password input
   *
   */
  function togglePassword() {
    if (type === "password") {
      setType("text");
    } else {
      setType("password");
    }
  }

  /**
   * toggle input eye svg
   *
   */

  function toggleEyeIcon() {
    if (type === "password") {
      return (
        <span className={svgClassRight} onClick={togglePassword}>
          <OpenEyeSvg />
        </span>
      );
    } else {
      return (
        <span className={svgClassRight} onClick={togglePassword}>
          <CloseEyeSvg />
        </span>
      );
    }
  }

  /**
   * handle input focus
   */
  function handleInputFocus() {
    setSvgClassLeft(
      " absolute inset-y-0 left-0 flex items-center text-default-700"
    );
    setSvgClassRight(
      "absolute inset-y-0 right-0 flex items-center text-default-700"
    );
  }

  /**
   * handle input blur
   */
  function handleInputBlur() {
    setSvgClassLeft("absolute inset-y-0 left-0 flex items-center");
    setSvgClassRight("absolute inset-y-0 right-0 flex items-center");
  }
  return (
    <div className="relative">
      <span className={svgClassLeft}>{props.children}</span>

      {props.type === "password" && toggleEyeIcon()}

      <input
        type={type}
        className="mt-1 h-10 block w-full border-gray-500 border border-b-2 form-input__bottom-border focus:outline-none focus:border-default-100 px-8 focus:text-default-700"
        placeholder={props.placeholder}
        onFocus={handleInputFocus}
        onBlur={handleInputBlur}
      />
    </div>
  );
}
