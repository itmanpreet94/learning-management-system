import React from "react";
import Headers from "../components/header/Header";

function Layout(props) {
  return (
    <React.Fragment>
      <Headers />
      {props.children}
    </React.Fragment>
  );
}
export default Layout;
