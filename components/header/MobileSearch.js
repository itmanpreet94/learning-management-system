import React from "react";
import Link from "next/link";
import Logo from "./Logo";

function MobileSearch(props) {
  return (
    <div>
      <div className="w-full absolute top-0 bg-white flex flex-col flex-1 h-screen overflow-y-hidden">
        <div className="relative flex items-center">
          <div className="py-3 px-2 items-center absolute z-40 right-0">
            <div className="">
              <button
                className="hover:cursor-pointer flex items-center "
                onClick={props.toggleSearch}
              >
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  width="24"
                  height="24"
                  viewBox="0 0 24 24"
                  fill="none"
                  stroke="currentColor"
                  strokeWidth="2"
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  className="feather feather-x"
                >
                  <line x1="18" y1="6" x2="6" y2="18"></line>
                  <line x1="6" y1="6" x2="18" y2="18"></line>
                </svg>
              </button>
            </div>
          </div>
          <Logo />
        </div>

        <div className="text-sm text-gray-700">
          <div className="relative px-3 py-2">
            <span className="absolute inset-y-0 left-0 flex items-center px-5">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                viewBox="0 0 24 24"
                fill="none"
                stroke="currentColor"
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
              >
                <circle cx="11" cy="11" r="8"></circle>
                <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
              </svg>
            </span>
            <input
              type="text"
              className="focus:outline-none focus:bg-white  bg-gray-300 h-10 w-full px-2 text-sm placeholder-gray-600 rounded border border-transparent focus:border-gray-400 appearance-none leading-normal font-thin px-10 py-5"
              placeholder="Search Courses"
            />
          </div>
          <div className="overflow-y-auto h-screen">
            <ul className="list-none">
              <li>
                <Link href="#">
                  <a className="block px-3 py-5 bg-gray-200 cursor-pointer border  border-b border-gray-300">
                    Search 3
                  </a>
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MobileSearch;
