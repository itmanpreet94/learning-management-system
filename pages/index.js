import "../styles/index.css";
import Layout from "../components/Layout";

export default () => {
  return (
    <Layout>
      <div className="min-h-screen bg-gray-200 mt-14">
        <section className="bg-default-100 text-white p-5">
          <p className="text-2xl font-semibold">
            Learn Together, Grow Together
          </p>
          <p className="text-md">
            Study any topic, anytime. Choose from thousands of expert-led
            courses now.
          </p>
          <button className="px-4 py-2 mt-6 bg-default-700 hover:bg-default-700 rounded text-white font-bold">
            Start Learning
          </button>
        </section>
        <section className=" text-center p-5">
          <p className="text-2xl font-medium ">
            Best place to learn Fluent English
          </p>
          <div className="flex justify-center ">
            <div className="text-md border-default-100 w-20 h-20 items-center justify-center rounded-full border-4 px-2 py-3 mx-2">
              <span className="block text-xl font-semibold"> 36</span>
              <span className="block text-md -mt-2 capitalize font-normal">
                courses
              </span>
            </div>
            <div className="text-md border-default-100 w-20 h-20 items-center justify-center rounded-full border-4 px-2 py-3 mx-2">
              <span className="block text-xl font-semibold"> 736</span>
              <span className="block text-md -mt-2 capitalize font-normal">
                Lessons
              </span>
            </div>
            <div className="text-md border-default-100 w-20 h-20 items-center justify-center rounded-full border-4 px-2 py-3 mx-2">
              <span className="block text-xl font-semibold"> 97</span>
              <span className="block text-md -mt-2 capitalize font-normal">
                Hours
              </span>
            </div>
          </div>
          <div>
            <div className="mt-5 bg-white rounded-lg overflow-hidden">
              <img
                className="h-56 w-full object-cover"
                src="https://images.unsplash.com/photo-1543109740-4bdb38fda756?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1534&q=80"
                alt="courses"
              />
              <div className="px-3 py-3">
                <div className=" text-left">
                  <div>
                    <span className="px-2 py-1 leading-none text-xs rounded-full bg-default-100 text-white uppercase font-semibold text">
                      Best Course
                    </span>
                    <span className="block mt-1 text-gray-900 text-sm font-semibold ">
                      English for Beginners: Intensive English Speaking Course
                    </span>
                  </div>
                  <div className=" mt-1 flex justify-between">
                    <div className="flex">
                      <span>
                        <svg
                          className="fill-current text-teal-600 h-4 w-4"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 24 24"
                        >
                          <polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
                        </svg>
                      </span>
                      <span>
                        <svg
                          className="fill-current text-teal-600 h-4 w-4"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 24 24"
                        >
                          <polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
                        </svg>
                      </span>
                      <span>
                        <svg
                          className="fill-current text-teal-600 h-4 w-4"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 24 24"
                        >
                          <polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
                        </svg>
                      </span>
                      <span>
                        <svg
                          className="fill-current text-teal-600 h-4 w-4"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 24 24"
                        >
                          <polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
                        </svg>
                      </span>
                      <span>
                        <svg
                          className="fill-current text-teal-600 h-4 w-4"
                          xmlns="http://www.w3.org/2000/svg"
                          viewBox="0 0 24 24"
                        >
                          <polygon points="12 2 15.09 8.26 22 9.27 17 14.14 18.18 21.02 12 17.77 5.82 21.02 7 14.14 2 9.27 8.91 8.26 12 2"></polygon>
                        </svg>
                      </span>
                    </div>
                    <span className="text-gray-800 text-sm">
                      4.2 (3434 Review)
                    </span>
                  </div>
                  <div className=" mt-1 flex justify-between">
                    <span className="flex justify-between items-center text-lg text-gray-900 mt-1 font-bold">
                      &#8377; 4500
                    </span>
                    <button className="bg-default-100 hover:bg-default-700 rounded px-5 text-base font-semibold text-white w-40 ">
                      Buy Now
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    </Layout>
  );
};
