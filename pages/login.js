import "../styles/index.css";
import Layout from "../components/Layout";
import { Textinputs } from "../components/form/Inputs";
import {
  LoginSvg,
  UserSvg,
  PasswordSvg,
  GoogleSvg,
  FacebookSvg
} from "../components/svg";
import Link from "next/link";

function login() {
  return (
    <Layout>
      <div className="min-h-screen mt-14">
        <section className=" text-center p-5">
          <h1 className="text-center text-2xl font-semibold leading-tight ">
            Welcome Back
          </h1>
          <div>
            <LoginSvg />
          </div>
          <div>
            <div className="py-5 text-gray-500">
              <Textinputs type="text" placeholder="Email ID or Mobile No.">
                <UserSvg />
              </Textinputs>
            </div>
            <div className="py-5 text-gray-500">
              <Textinputs type="password" placeholder="Password">
                <PasswordSvg />
              </Textinputs>
            </div>
          </div>
          <div className="text-right text-default-100 hover:text-default-700 font-semibold">
            <Link href="#">
              <a>Forgot Password</a>
            </Link>
          </div>
          <div className="items-center mt-5">
            <button className="bg-default-100 px-10 py-2 text-white font-semibold rounded-full focus:outline-none">
              Login
            </button>
          </div>
          <div className="flex justify-center mt-5">
            <Link href="#">
              <a className="mx-2">
                <GoogleSvg />
              </a>
            </Link>
            <Link href="#">
              <a className="mx-2">
                <FacebookSvg />
              </a>
            </Link>
          </div>
          <div className=" text-gray-500 mt-4">
            Don't have an account?
            <Link href="#">
              <a className="text-default-100 hover:text-default-700 font-semibold">
                {" "}
                Register
              </a>
            </Link>
          </div>
        </section>
      </div>
    </Layout>
  );
}

export default login;
